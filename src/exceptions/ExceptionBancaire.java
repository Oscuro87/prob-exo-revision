package exceptions;

/**
 * @author E.CAPELLE
 */
public class ExceptionBancaire extends Exception {
    private String errorMessage;

    public ExceptionBancaire(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }
}
