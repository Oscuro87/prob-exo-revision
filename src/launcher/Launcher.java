package launcher;

import marketcustomer.AccountHolder;
import marketcustomer.ShareAccount;

/*
 * @author E.CAPELLE
 */
public class Launcher {

    public static void main(String[] args) {
        ShareAccount acc1 = new ShareAccount(new AccountHolder("Test", "Dude"));
        System.out.println(acc1);

        acc1.buyShares("Delhaize", 100);
        System.out.println(acc1);

        acc1.sellShares("Delhaize", 200); // !! vendre trop
        System.out.println(acc1);

        acc1.buyShares("Delhaize", 1000); // !! pas assez d'argent
        System.out.println(acc1);

        acc1.buyShares("unknown", 100); // !! L'action n'existe pas
    }

}
