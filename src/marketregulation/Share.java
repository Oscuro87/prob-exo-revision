package marketregulation;

/**
 * @author E.CAPELLE
 */

public class Share implements Comparable<Share> {
    private final String name;
    private double price;

    public Share(String name, double startingPrice) {
        this.name = name;
        this.price = startingPrice;
    }

    public String getName() {
        return name;
    }

    public double getCurrentPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Nom : " + name + " | Prix : " + price;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Share) {
            Share other = (Share) obj;
            if (this.name.compareTo(other.getName()) != 0)
                return false;
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Share o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public int hashCode() {
        return name.hashCode() * 73;
    }
}
