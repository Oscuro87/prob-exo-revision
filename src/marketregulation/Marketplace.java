package marketregulation;

import exceptions.ExceptionBancaire;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author E.CAPELLE
 */
public class Marketplace {
    private static Marketplace marketplaceInstance;

    private List<Share> availableShares;

    private Marketplace() {
        availableShares = new ArrayList<Share>();
        populateShareList();
    }

    public static Marketplace getInstance() {
        if (marketplaceInstance == null)
            marketplaceInstance = new Marketplace();
        return marketplaceInstance;
    }

    private void populateShareList() {
        Random r = new Random();
        availableShares.add(new Share("Delhaize", 50d));
        availableShares.add(new Share("Colruyt", 30d));
        availableShares.add(new Share("Lidl", 20d));
    }

    public Share getShareByName(String name) throws ExceptionBancaire {
        for (Share s : availableShares)
            if (s.getName().equals(name))
                return s;
        throw new ExceptionBancaire("Aucune action ne porte le nom : " + name);
    }

    public String getShareListString() {
        StringBuilder list = new StringBuilder();
        for (Share share : availableShares)
            list.append(share.toString() + "\n");
        return list.toString();
    }

    private void updateSharesPrices() {
        //TODO: modification du prix des actions
    }
}
