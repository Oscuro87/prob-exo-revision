package marketcustomer;

import marketregulation.Share;

/**
 * @author E.CAPELLE
 */
public class ShareLine {
    private Share share;
    private int quantity;

    public ShareLine(Share share, int qty) {
        this.share = share;
        this.quantity = qty;
    }

    public Share getShare() {
        return share;
    }

    public String getShareName() {
        return share.getName();
    }

    public double getSharePrice() {
        return share.getCurrentPrice();
    }

    public int getQuantity() {
        return quantity;
    }

    void setQuantity(int newQty) {
        this.quantity = newQty;
    }

    @Override
    public String toString() {
        return "Nom : " + getShareName() + " | Prix : " + getSharePrice() + " | Qty.: " + getQuantity();
    }
}
