package marketcustomer;

import exceptions.ExceptionBancaire;
import marketregulation.Marketplace;
import marketregulation.Share;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Oscuro
 */

public class ShareAccount {
    // Statics
    private static long id_giver = 1;

    private final long id;
    private AccountHolder owner;
    private double money;
    private Set<ShareLine> ownedShares;

    public ShareAccount(AccountHolder owner) {
        this.id = id_giver;
        ++id_giver;
        this.owner = owner;
        money = 10000.d;
        ownedShares = new HashSet<ShareLine>();
    }

    public long getId() {
        return id;
    }

    public String getOwnerName() {
        return owner.getFullName();
    }

    public double getMoney() {
        return money;
    }

    public String getFormattedOwnedSharesString() {
        StringBuilder bld = new StringBuilder();
        bld.append("Titres possédés par cet compte:\n");

        if (ownedShares.size() != 0) {
            for (ShareLine line : ownedShares)
                bld.append("\t*" + line.toString());
        } else
            bld.append("Aucun titre possédé!");

        return bld.toString();
    }

    private ShareLine getShareLineFromShare(Share share) {
        Iterator<ShareLine> it = ownedShares.iterator();
        while (it.hasNext()) {
            ShareLine current = it.next();
            if (current.getShare().equals(share))
                return current;
        }

        return null;
    }

    public void buyShares(String shareName, int amount) {
        Share share = null;

        try {
            share = Marketplace.getInstance().getShareByName(shareName);
        } catch (ExceptionBancaire e) {
            System.out.println(e.getMessage());
            return;
        }

        double totalPrice = share.getCurrentPrice() * amount;
        ShareLine line = null;

        if (money < totalPrice) {
            System.out.println("Pas assez d'argent pour passer cette transaction!");
            return;
        }

        if ((line = getShareLineFromShare(share)) != null)
            line.setQuantity(line.getQuantity() + amount);
        else {
            line = new ShareLine(share, amount);
            ownedShares.add(line);
        }

        money -= totalPrice;
    }

    public void sellShares(String shareName, int amount) {
        Share share = null;

        try {
            share = Marketplace.getInstance().getShareByName(shareName);
        } catch (ExceptionBancaire e) {
            System.out.println(e.getMessage());
            return;
        }

        double profitTotal = share.getCurrentPrice() * amount;
        ShareLine line = null;

        if ((line = getShareLineFromShare(share)) != null) {
            if (line.getQuantity() < amount) {
                System.out.println("Vous n'avez pas assez de parts de cette action à vendre.");
                return;
            }
            line.setQuantity(line.getQuantity() - amount);
            if (line.getQuantity() == 0)
                ownedShares.remove(line);
        } else {
            System.out.println("Vous n'avez aucune part de cette action.");
            return;
        }

        money += profitTotal;
    }

    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();

        bld.append("\n");
        bld.append("ID du compte: " + id + "\n");
        bld.append("Nom du propriétaire : " + getOwnerName() + "\n");
        bld.append("Solde actuel : " + getMoney() + " €\n");
        bld.append(getFormattedOwnedSharesString());
        bld.append("\n");

        return bld.toString();
    }
}
